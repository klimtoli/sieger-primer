from math import remainder
import matplotlib.pyplot as plt
from sys import argv
from prime_utils import *

def plot_primes_reminders(a,b,show_table=True):
    prod = a * b
    print(f"The product of a={a} and b={b} is {a*b}")

    x_axis, y_axis = process_product(a*b)
    print(f"Table for {a*b}")
    if show_table:
        for i in range(len(x_axis)):
            print(f"{x_axis[i]}\t{y_axis[i]}")
    plt.figure()
    plt.plot(x_axis, y_axis,'b.-')
    tmp_flag_first = True
    for i in range(len(x_axis)):
        if (i < len(x_axis) - 3) and y_axis[i] > y_axis[i+1] < y_axis[i+2]:
            plt.plot(x_axis[i+1],y_axis[i+1],'yx')
        if y_axis[i]== 0:
            if tmp_flag_first:
                plt.plot(x_axis[i],y_axis[i],'r*', label="Prime number")
                tmp_flag_first = False
    plt.title("Remianders")
    plt.ylabel("remainder")
    plt.xlabel("divisor")
    plt.legend()
    plt.grid()

def plot_local_mins_dist(a,b,show_table=True):
    prod = a * b
    divs, rems = process_product(prod)
    mins = local_mins(rems, divs)
    dist = list()
    dist.append(0)
    for i in range(1,len(mins)):
        dist.append(mins[i]-mins[i-1])
    plt.figure()
    plt.plot(mins, dist,'.-')
    plt.grid()
    plt.title("Distance between local mins")
    plt.xlabel("local mins")
    plt.ylabel("Distance from previous local min")
    print("mins:",mins)
    print("divs:",divs)


def plot_diff(a,b,show_table=True):
    prod = a * b
    divs, rems = process_product(prod)
    # TODO implement something not so resource unfriendly
    diff = differentiate(rems)
    plt.figure()
    plt.plot(divs[:-1],diff,'.-')
    plt.title("Differentaion")
    plt.ylabel("Slope")
    plt.xlabel("Divs")
    plt.grid()
    


if __name__ == "__main__":
    if len(argv) == 3:
        a = int(argv[1])
        b = int(argv[2])
    else:
        a = int(input("Enter firts prime number: "))
        b = int(input("Enter second prime number: "))
    print(f"Given numbers are a={a} b={b}, their product is {a*b}")
    if not is_prime(a):
        print(f"The given number {a} is not prime!")
    elif not is_prime(b):
        print(f"The given number {b} is not prime!")

    plot_diff(a,b,show_table=False)
    plot_primes_reminders(a,b,show_table=False)
    plot_local_mins_dist(a,b,show_table=False)
    plt.show()
    