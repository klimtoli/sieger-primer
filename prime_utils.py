def is_prime(a):
    """Checks wether given number is prime."""
    i = 2
    while i*i <= a:
        if a % i == 0:
            return False
        i += 1
    return True

def differentiate(arr,step=1):
    """Simple forward differentiation with fixed step size of 1"""
    diff = list()
    for i in range(len(arr) - 1):
        diff.append((arr[i+step] - arr[i])/step)
    return diff

def local_mins(rems, divs):
    """Returns indicies of local minimals of an array `arr`."""
    assert len(rems) > 3
    loc_mins = list()
    for i in range(len(rems)-2):
        if rems[i] > rems[i+1] < rems[i+2]:
            loc_mins.append(divs[i+1])
    return loc_mins

def process_product(a):
    """Returns list of dividers and list of reminders"""
    reminders = list()
    dividers = list()
    for i in range(2, a):
        dividers.append(i)
        reminders.append((a % i)/i)
    return dividers, reminders