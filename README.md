# Sieger Primer

Decomposes a product of two prime numbers and visualise iterative division on a plot.

## How to run
Clone this repo. Use this programe just by calling `python3 main.py` or you can directly specify desired prime numbers when calling `python3 main.py prime_number1 prime_number2`.

## Requirements
- python3.9
- matplotlib